<?php 
ini_set("display_errors", "0");
error_reporting(E_ALL);
?>

<!DOCTYPE html>
<html>
    <div class="row">
        <div class="col-lg-4">
            <img src='./fotos/logo.jpg' width="130" alt='logo BRAVOS' style="margin-left:100px;">
        </div>
        <div class="col-lg-8" style="margin-top: 30px;">
            <h1>Grupo BRAVOS CAP</h1>
        </div>
    </div>
    <hr>
    <div class="container">
        
                 <div class="row">
                      <div class="col-sm-6">

                         <legend style="margin-top:30px">Nossas reuniões regulares</legend>
                           <div class="row">
                                <div class="col-sm-6">
                                    <!--p><b>B</b>atalhão de</p>
                                    <p><b>R</b>esgate e</p>
                                    <p><b>A</b>poio</p>
                                    <p><b>V</b>oluntário a</p>
                                    <p><b>O</b>bras</p>
                                    <p><b>S</b>ociais </p>-->
                                    <img src='./images/encontros.jpeg' width="100%" alt='encontros' >
                                   
                                </div>
                                       
                                 <div class="col-sm-6">
                                    <p style="text-align:justify">
                                        <b> Nossos encontros são aos Sábados, exceto o último do mês, às 9h na CAP, confira nossa programação e leve seu filho!</b> Aceitamos novos membros a partir de 6 anos.
                                        Confira também nossos parceiros!  Todos os que desejam ser um BRAVO, aliste-se, mas atenção, todas as informações são necessárias! Venha e convide um colega, Avante Bravos!</p>
                                   </div>   
                                   <div align="center">             
                                        <a  href='index.php?pagina=cadastro_publico' class="btn btn-primary" style="margin-top:30px; margin-bottom:30px;">Aliste-se já!</a>   
                                    </div>    
                                        <div class="sharethis-inline-share-buttons"></div>
                                 
                            </div>
                                   
                            </div>
                                
                            <div class="col-sm-6">
                                        <legend style="margin-top:30px">Localização:</legend>
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3837.5763118469386!2d-48.09386048514396!3d-15.87885038899878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTXCsDUyJzQzLjkiUyA0OMKwMDUnMzAuMCJX!5e0!3m2!1spt-BR!2sbr!4v1562120424607!5m2!1spt-BR!2sbr" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>

                                </div>

                                       
               </div> 
                      <hr>

        
        <div class="row" style="margin-top:20px">
         <legend>Informações Iniciais</legend>
         <div class="col-sm-3">
                     
                        <p style="font-size:20px; text-align:center; margin-top:5px">Clique aqui e conheça o nosso trabalho.</p>                  
                        <p style="font-size:12px; margin-top:5px;  text-align:center;"> 
                        <a href="./docs/BRAVOSCAP.pdf" download="" class="btn btn-primary">BRAVOS.PDF</a> </p> 
                        <p style="margin-top:5px;  text-align:center;"> 
                        <img src="fotos/WhatsApp-icone.png" width="20"> 61 98426.0515</p>
                       
                    <legend style="margin-top:5px; text-align:center;" >Se você já se alistou solicite sua carteirinha! </legend> 
                    <img src="images/carteirinha.JPG" width="100%" >
                </div>
            <div class="col-sm-3">

                    <legend>Nossas Atividades</legend>
                    <li>Atividades Missionárias e Comunitárias</li>
                    <li>Atividades sobre Profissões</li>
                    <li>Habilidades Domésticas</li>
                    <li>Atividades Recreativas</li>
                    <li>Atividades Agrícolas</li>
                    <li>Habilidades Manuais</li>
                    <li>Ciência e Saúde</li>
                    <li>Estudo da Natureza</li>
                    <li>Educação física</li>
                    <li>Ordem unida</li>
                    <li>Nós é amarrações</li>
                    <li>Atividades em grupo</li>
                    <li>Cantiga de correr</li><br>

                </div>
 
                <div class="col-sm-3">
                <img src="images/material.jpeg"  width="200" height="" >
                </div>
 
                <div class="col-sm-3">
                <legend>Material Básico</legend>
                        <li>3m de corda de 10mm</li>
                        <li>2m de corda de 6mm</li>
                        <li>12m de corda de 10mm</li>
                        <li>2x 6m de corda de 5mm</li>
                        <li>2 mosquetão</li>
                        <li>1 garrafinha de alumínio</li>
                        <li>1 caneco de alumínio</li>
                        <li>1 bolsa de chuteira</li>
                        <li>Protetor solar</li>
                        <li>Barraca de 2 homens</li>
                        <li>Talheres</li>
                        <li>Colher</li>
                        <li>Lanterna</li>
                        <li>Repelente</li>
                    </div>

        
    </div>
</html>