

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 

    <title>Login · BRAVOS</title>
   

    <!-- Custom styles for this template -->
    <link href="assets/css/login.css" rel="stylesheet">
  </head>




 <div class="container">
  
  <body class="text-center">
    <form class="form-signin">
    <img src='fotos/log_transparent.png' width="130" alt='logo BRAVOS' style="margin-center:100px;">
      <h1 class="h3 mb-3 font-weight-normal">Login</h1>
      <?php //flashMsg("warning");?>
      <?php //flashMsg("danger");?>

      <div id="mensagens"></div>

      <label for="email" class="sr-only">E-mail</label>
      <input type="email" id="email" name="email" class="form-control" placeholder="Endereço de e-mail" autofocus>
      
      <label for="senha" class="sr-only">Senha</label>
      <input type="password" name="senha" id="senha" class="form-control" placeholder="Informa a senha">
      
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="1" name="lembrar"> Lembrar de mim
        </label>
      </div>
      
      <span class="g-signin2" data-onsuccess="onSignIn"></span>
      <br> 
      <button class="btn btn-lg btn-primary btn-block" type="button" id="bt_login" onclick="enviar()">Entrar</button>       
    </form>
    </div>    
  </body>
</html>