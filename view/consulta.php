<?php  
include "controller/verifica_logado.php";
include "model/ConsultaModel.php";?>

<!DOCTYPE html>
<html>
<head>
  	<title>Membros</title>
	<link rel="stylesheet" type="text/css" href="../assets/css/pure-min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom.css">
	<meta charset="UTF-8">
	<script src="https://apis.google.com/js/platform.js" async defer></script>
		<meta name="google-signin-client_id" content="655639225247-n2lcpvsp139dmhvafq37fo9r661vongv.apps.googleusercontent.com">
</head>

<nav class="navbar navbar-defaut">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Administrativo</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Restrito
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
               <li><a class="dropdown-item" href="?pagina=cadastrar">Cadastro ADM</a></li>
               <li> <a class="dropdown-item"  href="?pagina=alterar">Alterar ADM</a></li>                       
               <li> <a class="dropdown-item"  href="?pagina=consulta_adm">Consulta ADM</a></li>
               <li role="separator" class="divider"></li>
               <li> <a class="dropdown-item" href="?pagina=acampamento">Acampamento</a></li> 
               <li role="separator" class="divider"></li>
               <li> <a class="dropdown-item"  href="?pagina=remessa">Remessa</a></li>
               <li> <a class="dropdown-item"  href="?pagina=rem">Pedido</a></li>
               <li> <a class="dropdown-item"  href="?pagina=producao">Produção</a></li> 
               <li> <a class="dropdown-item"  href="?pagina=mala_direta">Mala Direta</a></li>
               <li> <a class="dropdown-item"  href="?pagina=consulta">Consulta BRAVOS</a></li>     
        </ul>
      </li>
      <li><a href="?pagina=transparencia">Upload</a></li> 
      <li><a href="?pagina=perfil">Perfil</a></li> 
      <li><a href="?pagina=logout">Logout</a></li> 
    </ul>
  </div>
</nav>






<div align='center'><img src='fotos/log_transparent.png' width="130" alt='logo BRAVOS'></div>

<?php var_dump($nome); if($nome != null){ ?><a style="font-size:9px;" href="index.php?pagina=logout"><?=$saudacao?> <?=$sair?> </a> <?php } ?>



                                <fieldset>
										<!-- Cabeçalho da Listagem -->
										<legend><h1>Relação de Membros</h1></legend>
										<!-- Formulário de Pesquisa -->
										<form action="/index.php?pagina=consulta" method="post" id='form-contato' class="form-horizontal col-md-10">
											<div class="row">
												<div class="col-lg-6">
													<div class="input-group">
													<input type="text" class="form-control" id="termo" name="termo" placeholder="Infome o Nome ou E-mail ou Endereço">
													<span class="input-group-btn">
														<button type="submit" class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Pesquisar</button>
													</div>
													</span>
												</div>

												<div class='col-md-4'>
												<button type="submit" class="btn btn-default btn-sm"><a href='/index.php?pagina=relatorio_membros'><span class="glyphicon glyphicon-download-alt"></span></a></button>
												<button type="submit" class="btn btn-default btn-sm"><a href='/index.php?cadastro'><span class="glyphicon glyphicon-plus"></span></a></button>

												<button type="submit" class="btn btn-default btn-sm"><a href='/index.php?pagina=aniversariantes'><span class="glyphicon glyphicon-gift"></a></button>
												</div>
											</div>
										</form>
										<?php foreach($total as $ttl):?>
										<p>Total de registros:<?=$ttl->total_registros?></p>
										<?php endforeach;?>
							<!--<div class='clearfix ' style="overflow: auto; width: 1200px; height: 300px; border:solid 0px">

											<?php if(!empty($clientes)):?>
											 Tabela de Clientes -->
														<!--table class="table" style="width:1140px" border="0"-->
									<div class="table table-responsive" >
										<table class="table">
											<thead>
															<tr class='active'>
															<th class="col-md-auto" scope="col">Foto</th>
															<th class="col-md-auto" scope="col">Nome</th>
															<!--th-- class="col-md-auto" scope="col">E-mail</!--th-->
															<th class="col-md-auto" scope="col">Endereço</th>
															<th class="col-md-auto" scope="col">Nascimento</th>
															<th class="col-md-auto" scope="col">Celular</th>
															<th class="col-7 col-md-1" scope="col">Ação</th>
															<!--th class="col-md-auto" scope="col">Ação</th-->
															</tr>
														</thead>
													<?php foreach($clientes as $cliente):?>
													
													<tbody>
													<tr>
													<td><img src='fotos/<?=$cliente->foto?>' height='' width='30'></td>
														<td class="col-md-auto"><?=$cliente->nome?></td>
														<!--td-- class="col-md-auto"><?=$cliente->email?></!--td-->
														<td class="col-md-auto"><?=$cliente->endereco?></td>
														<?php $date1 = strtr($cliente->dt_nascimento, '/', '-'); ?>
														<td class="col-md-auto"><?=date('d/m/Y', strtotime($date1))?></td>
														<td class="col-md-auto"><?=$cliente->celular?></td>
														<td class="col-7 col-md-1">
														<!--td class="col-md-auto"-->
														<a href='carteira.php?id=<?=$cliente->id?>'><span class="glyphicon glyphicon-user"  title="Visualizar Registro"></span></a>
														<a href='editar.php?id=<?=$cliente->id?>'><span class="glyphicon glyphicon-edit" title="Editar Registro"></span></a>
														<a href='javascript:void(0)' class="link_exclusao" rel="<?=$cliente->id?>"><span class="glyphicon glyphicon-remove" title="Excluir Registro"></span></a>
														</td>
														</tr>
													</tbody>
												<?php endforeach;?>
										</div>
									</table>
										<?php else: ?>
<!-- encaixe-->
										<div class="dataTables_paginate paging_two_button" id="contacts_paginate">
										<a class="paginate_disabled_previous" tabindex="0" role="button" id="contacts_previous" aria-controls="contacts">Anterior</a>
										<a class="paginate_disabled_next" tabindex="0" role="button" id="contacts_next" aria-controls="contacts">Seguinte</a>
										</div>


<!-- encaixe-->
									<!-- Mensagem caso não exista clientes ou não encontrado  -->
									<div class="row">
										<div class="col-sm-12">
										<span>
											<h4 class="text-center text-primary">Não existem registros baseados nos filtros informados!</h4>
											</span>
										</div>
									</div>
									<?php endif; ?>
							</fieldset>
						</div>
		</div>


	<script type="text/javascript" src="../assets/js/custom.js"></script>
</html>