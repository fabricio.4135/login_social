<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>BRAVOS CAP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- JS Comportament -->
    <script src="./assets/js/comportamento.js"></script>
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" src="./assets/css/estilo.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">
    <!-- Styles Galeria -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    
</head>
<div class="row">
    <div class="col-lg-4">
        <img src='./fotos/logo.jpg' width="130" alt='logo BRAVOS' style="margin-left:100px;">
    </div>
    <div class="col-lg-8" style="margin-top: 30px;">
        <h1>Prestação de Contas</h1>
    </div>
</div>
<hr>

<div class="container">

<div id="row" class="row  row-fluid ">
    <div class=" column_container col-sm-2 ">
        <div class="column-inner ">
            <div class="wwrapper">
                <div class="textbox ">
                    <div class="textbox-inner" style="text-align: center;">
                        <div class="textbox-content" style="background-color: #f4f6f7;background-position: center top;padding-top: 45px;">
                        <div class="centered-box">
                            <div class="icon icon-pack-material icon-size-medium  icon-shape-square simple-icon" style="opacity: 1;">
                            <div class="icon-inner" style="">
                            <span class="glyphicon" style='font-size:48px;'>&#xe032;</span>
                            </div>
                        </div>
                    </div>
                    <div class="clearboth">

                    </div>
                    <div class="divider " style="margin-top: 19px;">
                </div>
                    <div class="row  inner row-fluid">
                        <div class="column column_container col-sm-12">
                            <div class="column-inner ">
                                <div class="wrapper">
                
                                        <div class="text_column content_element ">
                                            <div class="wrapper">
                                                <h4 style="text-align: center;"><strong>ANUAL<br>2018</strong></h4>

                                            </div>
                                        </div>

                                    <div class="btn-container btn-position-center">
                                    <a style="margin-top:10px; margin-bottom:10px" href="?pagina=2018" class="btn btn-default
                                            btn-lg" >Acessar</a>
                                    </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        
        
        
        
        
        
   
        <div class=" column_container col-sm-2 ">
        <div class="column-inner ">
            <div class="wwrapper">
                <div class="textbox ">
                    <div class="textbox-inner" style="text-align: center;">
                        <div class="textbox-content" style="background-color: #f4f6f7;background-position: center top;padding-top: 45px;">
                        <div class="centered-box">
                            <div class="icon icon-pack-material icon-size-medium  icon-shape-square simple-icon" style="opacity: 1;">
                            <div class="icon-inner" style="">
                            <span class="glyphicon" style='font-size:48px;'>&#xe032;</span>
                            </div>
                        </div>
                    </div>
                    <div class="clearboth">+-

                    </div>
                    <div class="divider " style="margin-top: 19px;">
                </div>
                    <div class="row  inner row-fluid">
                        <div class="column column_container col-sm-12">
                            <div class="column-inner ">
                                <div class="wrapper">
                
                                        <div class="text_column content_element ">
                                            <div class="wrapper">
                                                <h4 style="text-align: center;"><strong>ANUAL<br>2019</strong></h4>

                                            </div>
                                        </div>

                                    <div class="btn-container btn-position-center">
                                    <a style="margin-top:10px; margin-bottom:10px" href="?pagina=2019" class="btn btn-default
                                            btn-lg" >Acessar</a>
                                    </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        
        



        <div class=" column_container col-sm-2 ">
        <div class="column-inner ">
            <div class="wwrapper">
                <div class="textbox ">
                    <div class="textbox-inner" style="text-align: center;">
                        <div class="textbox-content" style="background-color: #f4f6f7;background-position: center top;padding-top: 45px;">
                        <div class="centered-box">
                            <div class="icon icon-pack-material icon-size-medium  icon-shape-square simple-icon" style="opacity: 1;">
                            <div class="icon-inner" style="">
                            <span class="glyphicon" style='font-size:48px;'>&#xe032;</span>
                            </div>
                        </div>
                    </div>
                    <div class="clearboth">

                    </div>
                    <div class="divider " style="margin-top: 19px;">
                </div>
                    <div class="row  inner row-fluid">
                        <div class="column column_container col-sm-12">
                            <div class="column-inner ">
                                <div class="wrapper">
                
                                        <div class="text_column content_element ">
                                            <div class="wrapper">
                                                <h4 style="text-align: center;"><strong>ANUAL<br>2020</strong></h4>

                                            </div>
                                        </div>

                                    <div class="btn-container btn-position-center">
                                    <a style="margin-top:10px; margin-bottom:10px" href="?pagina=2020" class="btn btn-default
                                            btn-lg" >Acessar</a>
                                    </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        
        


        <div id="row" class="row  row-fluid ">
    <div class=" column_container col-sm-2 ">
        <div class="column-inner ">
            <div class="wwrapper">
                <div class="textbox ">
                    <div class="textbox-inner" style="text-align: center;">
                        <div class="textbox-content" style="background-color: #f4f6f7;background-position: center top;padding-top: 45px;">
                        <div class="centered-box">
                            <div class="icon icon-pack-material icon-size-medium  icon-shape-square simple-icon" style="opacity: 1;">
                            <div class="icon-inner" style="">
                            <span class="glyphicon" style='font-size:48px;'>&#xe032;</span>
                            </div>
                        </div>
                    </div>
                    <div class="clearboth">

                    </div>
                    <div class="divider " style="margin-top: 19px;">
                </div>
                    <div class="row  inner row-fluid">
                        <div class="column column_container col-sm-12">
                            <div class="column-inner ">
                                <div class="wrapper">
                
                                        <div class="text_column content_element ">
                                            <div class="wrapper">
                                                <h4 style="text-align: center;"><strong>ANUAL<br>2021</strong></h4>

                                            </div>
                                        </div>

                                    <div class="btn-container btn-position-center">
                                    <a style="margin-top:10px; margin-bottom:10px" href="?pagina=2021" class="btn btn-default
                                            btn-lg" >Acessar</a>
                                    </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        
 
   
        <div class=" column_container col-sm-2 ">
        <div class="column-inner ">
            <div class="wwrapper">
                <div class="textbox ">
                    <div class="textbox-inner" style="text-align: center;">
                        <div class="textbox-content" style="background-color: #f4f6f7;background-position: center top;padding-top: 45px;">
                        <div class="centered-box">
                            <div class="icon icon-pack-material icon-size-medium  icon-shape-square simple-icon" style="opacity: 1;">
                            <div class="icon-inner" style="">
                            <span class="glyphicon" style='font-size:48px;'>&#xe032;</span>
                            </div>
                        </div>
                    </div>
                    <div class="clearboth">

                    </div>
                    <div class="divider " style="margin-top: 19px;">
                </div>
                    <div class="row  inner row-fluid">
                        <div class="column column_container col-sm-12">
                            <div class="column-inner ">
                                <div class="wrapper">
                
                                        <div class="text_column content_element ">
                                            <div class="wrapper">
                                                <h4 style="text-align: center;"><strong>ANUAL<br>2022</strong></h4>

                                            </div>
                                        </div>

                                    <div class="btn-container btn-position-center">
                                    <a style="margin-top:10px; margin-bottom:10px" href="?pagina=2022" class="btn btn-default
                                            btn-lg" >Acessar</a>
                                    </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        
        



        <div class=" column_container col-sm-2 ">
        <div class="column-inner ">
            <div class="wwrapper">
                <div class="textbox ">
                    <div class="textbox-inner" style="text-align: center;">
                        <div class="textbox-content" style="background-color: #f4f6f7;background-position: center top;padding-top: 45px;">
                        <div class="centered-box">
                            <div class="icon icon-pack-material icon-size-medium  icon-shape-square simple-icon" style="opacity: 1;">
                            <div class="icon-inner" style="">
                            <span class="glyphicon" style='font-size:48px;'>&#xe032;</span>
                            </div>
                        </div>
                    </div>
                    <div class="clearboth">

                    </div>
                    <div class="divider " style="margin-top: 19px;">
                </div>
                    <div class="row  inner row-fluid">
                        <div class="column column_container col-sm-12">
                            <div class="column-inner ">
                                <div class="wrapper">
                
                                        <div class="text_column content_element ">
                                            <div class="wrapper">
                                                <h4 style="text-align: center;"><strong>ANUAL<br>2023</strong></h4>

                                            </div>
                                        </div>

                                    <div class="btn-container btn-position-center">
                                    <a style="margin-top:10px; margin-bottom:10px" href="?pagina=2023" class="btn btn-default
                                            btn-lg" >Acessar</a>
                                    </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>










    </div>

</div>


</html>