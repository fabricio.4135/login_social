
function enviar() {
  const dados = {
    "email": document.getElementById("email").value,
    "senha": document.getElementById("senha").value,
    "lembrar": document.getElementById("email").checked,
  };

  validarLogin(dados);
}

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();

  const dados = {
    user_id: profile.getId(),
    userName: profile.getName(),
    userPicture: profile.getImageUrl(),
    email: profile.getEmail(),
    givenName: profile.getGivenName(),
    familyName: profile.getFamilyName(),
    senha: null
  };

  validarLogin(dados);
}

function validarLogin(dados) {
  fetch("controller/logar_ajax.php", {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(dados)
  }).then(response => {
    // console.log(response);
    // if (response.status == 200) {
    //   window.location.href = 'index.php?pagina=consulta';
    // } else {
    //   let divMsg = document.getElementById('mensagens');
    //   divMsg.innerHTML = `<div class='alert alert-${result.status}'>${result.msg}</div>`;
    // }
    response.json().then(valor => {
      console.log(valor);
      if (valor.status == 'success') {
        window.location.href = 'index.php?pagina=consulta';
      } else {
        let divMsg = document.getElementById('mensagens');
        divMsg.innerHTML = `<div class='alert alert-${result.status}'>${result.msg}</div>`;
      }
    })
  }).catch(function (error) {
    console.log("Falha ao executar a requisição " + error.message);
  })
}
