<?php 
ini_set("display_errors", 1);
error_reporting(E_ALL);
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>BRAVOS CAP</title>
    <meta name="google-site-verification" content="k3qfF6H52rtE3RWRs5SMXBt90HOl4yVKhFmyeILfQfA" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- JS Comportament -->
    <script src="assets/js/comportamento.js"></script>
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" src="assets/css/estilo.css">
    <link rel="stylesheet" type="text/css" href="assets/css/pure-min.css">
    <!--link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"-->
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    
    <!-- Custom styles for this template -->
    <link href="assets/js/carousel.css" rel="stylesheet">
    <!-- Styles Galeria-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/346a5abb17.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=601a28315692e4001147c3da&product=inline-share-buttons" async="async"></script>
	 <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id" content="655639225247-n2lcpvsp139dmhvafq37fo9r661vongv.apps.googleusercontent.com">
   <script src="assets/js/funcoes.js"></script>


    <style type="text/css">
	img{border-radius: 10px;}
    </style> 
    
    <title>Membros</title>
    </head>
<body>


<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                     data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                  </button>

           </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">                
                <!--<li class="active"><a href="index.php">Home <span class="sr-only">(current)</span></a></li>-->
                
                  <li> <a class="" href="#">
                     <img alt="Brand" src="fotos/log_transparent.png" width="20" height="" style=" margin-top: 0px; margin-left: 0px;"></a>
                  </li> 
                
                
                <li><a href="index.php">Home</a></li>             
                <li><a href="?pagina=programacao">Programação</a></li> 
                <li><a href="?pagina=parceiros">Parceria PMDF</a></li>              
                <li><a href="?pagina=patrocinio">Patrocinio</a></li> 
                <li><a href="?pagina=loja">Loja</a></li>         
                        
                  <li class="dropdown">                   
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">BRAVOS
                     <span class="caret"></span></a>                                   
                       
                     <ul class="dropdown-menu">                             
                           <li><a class="dropdown-item" href="?pagina=tarefas">Tarefas</a></li>            
                           <li role="separator" class="divider"></li>
                           <li><a class="dropdown-item" href="?pagina=civismo">Civismo</a></li>  
                           <li><a class="dropdown-item" href="?pagina=hinos_cancoes">Hinos e Canções</a></li>  
                           <li role="separator" class="divider"></li>
                           <li><a class="dropdown-item" href="?pagina=escotismo">Escotismo</a></li>
                           <li><a class="dropdown-item" href="?pagina=hinos">Hinos</a></li>                    
                           <li role="separator" class="divider"></li>
                           <li><a class="dropdown-item" href="?pagina=olimpiada">Olimpíada</a></li> 
                           <li><a class="dropdown-item" href="?pagina=galeria">Galeria</a></li>
                           <li> <a class="dropdown-item" href="?pagina=historico">Histórico</a></li>
                           <li><a class="dropdown-item" href="?pagina=aniversariantes">Aniversariantes</a></li>                     
                        </ul>
               
                   </li> 
                        
                     <li class="dropdown">   
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Transparência<span class="caret"></span></a> 
                              <ul class="dropdown-menu">
                                 <li><a href="?pagina=transparencia">Prestação de contas</a></li> 
                                 
                              </ul>
                     </li>    

                   <li class="dropdown">  
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GUARDIÕES<span class="caret"></span></a>       
                        <ul class="dropdown-menu"> 
                           <li><a href="?pagina=quem_somos">Quem Somos</a></li> 
                           <li><a href="?pagina=projetos">Projetos</a></li> 
                           <li><a href="?pagina=colaboradores">Colaboradores</a></li> 
                           <li><a href="?pagina=contato">Contato</a></li>  
                        </ul>
                  </li> 
                
                  
                                                                             
                  
                        
                           <li><a class="dropdown-item" href="?pagina=login"><b>LOGIN</b></a></li> 

                      
                       
                         

                 


            <?//php  echo $_SESSION['userName']; ?>
		

            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container- -->
</nav>


    <div class="container">

        <?php

   switch(@$_REQUEST['pagina']){


      case 'upload':
         include 'planilha.php';
         break;

      case 'transparencia':
         include 'prestacaoContas.php';
         break;

      case '2018':
            include '2018.php';
           break; 

      case '2019':
         include '2019.php';
        break;   
      
      case '2020':
         include '2020.php';
      break;    

      case '2021':
        include '2021.php';
       break;  
      
       case '2022':
         include '2022.php';
        break; 

      case 'remessa':
         include 'remessa.php';
         break;

       case 'login':
           include 'view/login.php';
           break;

           case 'aniversariantes':
           include 'aniversariantes_mes.php';
           break;

           case 'patrocinio':
            include 'patrocinio.php';
            break;

            case 'projetos':
               include 'projetos.php';
               break;

         case 'carteira':
               include 'carteira.php';
               break;

        case 'cadastro':
           include 'cadastro.php';
           break;

         case 'consulta':
           include 'view/consulta.php';
           break;
          
           case 'tarefas':
            include 'tarefas.php';
            break;
         case 'consulta_adm':
            include 'adm.php';
            break;
            
         case 'relatorio': 
            include  'relatorio_membros.php';
            break;

             case 'rolema':
               include 'rolema.php';
               break;

         case 'pedido':
               include 'pedido.php';
               break;

        case 'cadastro_publico':
           include 'cadastro_publico.php';
           break;

        case 'cadastrar':
           include 'cadastrar.php';
           break;

        case 'alterar':
           include 'alterar.php';
           break;

        case 'editar':
           include 'editar.php';
           break;

           case 'editar':
           include 'editar_publico.php';
           break;

       case 'escotismo':
           include 'escotismo.php';
           break;

        case 'galeria':
           include 'galeria.php';
           break;

        case 'parceiros':
           include 'parceiros.php';
           break;

        case 'programacao':
           include 'programacao.php';
           break;

           case 'historico':
            include 'historico.php';
            break; 
            
           case 'hinos':
            include 'hinos.php';
            break; 
            
        case 'olimpiada':
           include 'olimpiada.php';
           break;

        case 'loja':
           include 'loja.php';
           break;

           case 'producao':
            include 'producao.php';
            break;
 
           case 'editar_loja':
            include 'editar_loja.php';
            break;

        case 'contato':
           include 'contato.php';
           break;

           case 'hinos_cancoes':
            include 'hinos_cancoes.php';
            break;

            case 'civismo':
               include 'civismo.php';
               break; 

               case 'xadrez':
                  include 'xadrez.php';
                  break; 
                  
               case 'nos':
                  include 'nos.php';
                  break; 

        case 'mala_direta':
            include 'view/mala_direta.php';
            break;   

        case 'quem_somos':
           include 'quem_somos.php';
           break;

        case 'condominio':
           include 'condPM.php';
           break;

        case 'cardapio':
           include 'cardapio.php';
           break;

        case 'acampamento':
           include 'acampamento.php';
           break;

        case 'logout':
           include 'logout.php';
           break;

           case 'colaboradores':
            include 'colaboradores.php';
            break;           

        default:
           include 'view/home.php';
           break;

   }




        ?>

</body>


<!--Rodapé da Pagina-->
<div id="footer" padding="">
    <hr>
    <div id="" align="center">
        <address id="Administração-CAP-copyright">
            <address id="version">
                <p> Powered by <a href="https://casadeadoracaoprofetica.org.br/"
                        title="Sistema de gerenciamento Administrativo">
                         <img src="fotos/cap_transparente.png"  width="30" height="" alt="Instagram" style=" margin-top: 10px;"> CAP &reg; - </a> Ministério de Tecnologia CAP
                    2020
                    <address id="webmaster-contact-information"> Contato dos <a href="mailto:fabricio.4135@gmail.com.br"
                            title="Entre em contato com o webmaster via e-mail."> Desenvolvedores </a> para sugestões
                    </address>
                </p></a>

               <!-- ShareThis BEGIN --><div class="sharethis-inline-follow-buttons"></div><!-- ShareThis END -->

                <!--div-- class="jeg_sharelist">
                
                     <a href="https://www.instagram.com/bravoscap?r=nametag" class="jeg_btn-facebook expanded">
                     <i class="fa fa-instagram" aria-hidden="true"></i><span>Instagram</span></a>&nbsp;

                     <a href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fbravoscap.org.br" class="jeg_btn-facebook expanded">
                     <i class="fa fa-facebook-official"></i><span>Compartilhar</span></a>&nbsp;
                     
                     <a href="https://twitter.com/intent/tweet?text=BRAVOSCAP&amp;url=http%3A%2F%2Fbravoscap.org.br" class="jeg_btn-twitter expanded">
                     <i class="fa fa-twitter"></i><span>Tweet</span></a>&nbsp;
                     
                     <a href="https://api.whatsapp.com/send?phone=5561984260515&text=Bem%20vindos%20ao%20bravoscap.org.br" 
                     class="jeg_btn-whatsapp expanded">
                     <i class="fa fa-whatsapp"></i><span>Enviar</span></a>&nbsp;
                     
                     <a href="http://www.linkedin.com/shareArticle?url=http%3A%2F%2Fbravoscap.org.br" class="jeg_btn-linkedin expanded">
                     <i class="fa fa-linkedin"></i><span>Compartilhar</span></a>&nbsp;
                     
                     <a href="https://www.pinterest.com/pin/create/bookmarklet/?pinFave=1&amp;url=http%3A%2F%2Fbravoscap.org.br" class="jeg_btn-pinterest expanded">
                     <i class="fa fa-pinterest"></i><span>Pin</span></a>
                  </-div-->

    </div>
</div>

</html>