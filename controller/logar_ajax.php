<?php

require_once "funcoes.php";

include("../model/Login.php");

$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

$dados;

if ($contentType === "application/json") {
  //Receive the RAW post data.
  $content = trim(file_get_contents("php://input"));

  $html = html_entity_decode($content);

  print_r("HTML => $html");

  $dados = json_decode($content, true);

  echo json_last_error(); 

  print_r("Dados -> $dados");
}

if ($dados != null) {
	$credenciais = new Login($dados);
	$decript =  $dados['senha'] == null ? null : sha1($dados['senha']);  
	$credenciais->setSenha($decript); 	
	$ajax = new LogarAjax($credenciais);
	$resultado = $ajax->verificarTipoLogin();
	
	// Cabecalho da resposta
	header("Content-type: application/json");

	// Resposta convertida para Json
	echo json_encode($resultado);
}


/**
* Classe que representa as ações para efetuar o Login via ajax
*
*/
class LogarAjax {

	// Atributo que representa a Classe Login
	private $loginClass;

	/**
	* Construtor
	* @param $loginClass
	*/
	public function LogarAjax($login) {
		$this->setLoginClass($login);
	}

	/**
	* Getter e Setter
	*
	*/
	public function getLoginClass() {
		return $this->loginClass;
	}

	public function setLoginClass($login) {
		$this->loginClass = $login;
	}


	/**
	* Verifica o tipo de Login que será feito com base no parametros da classe Login
	*
	*/
	public function verificarTipoLogin() {

		var_dump($this->getLoginClass());

		$email = $this->getLoginClass()->getEmail();
		$senha = $this->getLoginClass()->getSenha();
		$userId = $this->getLoginClass()->getUserId();
		$lembrar = $this->getLoginClass()->isLembrar();

		if (!empty($email) && !empty($senha)) {
			return $this->logarFormulario($email, $senha, $lembrar);

		} else if (!empty($email) && !empty($userId)) {
			return $this->logar($email);

		}
		
		return $this->semDados();
	}

	/**
	* Login com a rede social Google
	*
	*/
	private function logar($email) {

		$logado = Funcoes::logar($email, null, false, null);
		
	    if ($logado == true) {
    	   return $retorno = [
   			    'status' => 'success',	            
    	        'msg' => 'Bem vindo,'.$_SESSION['usuario'],
				'nivel' => $_SESSION['niveis_acesso_id']
	        ];
	    }

	    return $this->loginInvalido();
	}

	/**
	* Login padrão por parametro
	*
	*/
	private function logarFormulario($email, $senha, $lembrar) {

	    $logado = Funcoes::logar($email, $senha, $lembrar);

	    if ($logado == true) {   	   
		   return $retorno = [
			'status' => 'success',	            
		 	'msg' => 'Bem vindo,'.$_SESSION['usuario'],
		 	'nivel' => $_SESSION['niveis_acesso_id']
	 		];
	    } 

	    return $this->loginInvalido();
	}
	
	private function semDados() {

	    return $retorno = [
    		'status' => 'danger',
    		'msg' => "Informe e-mail e senha para acessar!"
		];
	}

	private function loginInvalido() {

        return $retorno = [
            'status' => 'warning',
            'msg' => "E-mail e/ou senha inválidos!"
        ];
	}
	
}

