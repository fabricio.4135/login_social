<?php

require_once "conexao.php";

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

define("LOG_STATUS_FAIL", 0);
define("LOG_STATUS_OK", 1);

define("TENTATIVAS_LOGIN_FAIL", 3);
define("MINUTOS_LIMITE_LOGIN", 30);

class Funcoes {

    public static function flashMsg($id, $msg = null) {
        
        if (is_null($msg)) {
            echo  Funcoes::temFlashMsg($id) ? "<div class='alert alert-$id'>{$_SESSION['flashMsg'][$id]}</div>" : "";
            unset($_SESSION['flashMsg'][$id]);
        } else {
            $_SESSION['flashMsg'][$id] = $msg;
        }
    }

    public static function temFlashMsg($id) {
        return isset($_SESSION['flashMsg'][$id]) ? true : false;
    }

    public static function logar($email, $senha, $lembrar, $token = null) {
        $conn = conexao::getInstance();
        if (is_null($token)) {
            $sql = "select * from usuarios where 1 = 1";
            if ($email!=null && $senha==null){
                $sql .= " and email = :email";
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(":email", $email);
            } else if ($email!=null && $senha!=null) {               
                $sql .= " and email = :email and senha = :senha";
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(":email", $email);
                $stmt->bindParam(":senha", $senha);         
            }
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
           
            if ($lembrar == true) {
                setcookie('lembrar', $row['token'], time() + (60 * 60 * 24 * 30));
            }
        
            $_SESSION['usuario'] = $row['usuario'];
            $_SESSION['niveis_acesso_id'] = $row['niveis_acesso_id'];
            
            return true;
        } else {
            $sql = "select * from usuarios where token = :token";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":token", $token);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $condicao = !empty($row);
        }

        if ($condicao == true) {
            unset($row['senha']);
            $_SESSION['usuario'] = $row;
            Funcoes::logAcesso($email, LOG_STATUS_OK);

            return true;
        }

        Funcoes::logAcesso($email, LOG_STATUS_FAIL);

        return false;
    }

    public static function logAcesso($email, $tipo) {
        $conn = conexao::getInstance();
        $sql = "insert into logs (email, tipo) values (:email, :tipo);";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(":email", $email);
        $stmt->bindParam(":tipo", $tipo);

        return $stmt->execute();
    }

    public static function getUltimoAcesso($conn, $email) {

        $sql = "SELECT 
            COUNT(id) AS tentativas, email, data_hora
            FROM
                logs
            WHERE
                email = :email
                    AND data_hora > (SELECT MAX(NOW()) - INTERVAL 30 MINUTE)
                    AND tipo = 0
            GROUP BY email limit 1;";

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(":email", $email);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public static function sair($redirecionar = null) {

        setcookie('lembrar', null, time() - 100);
        session_destroy();

        if (!is_null($redirecionar)) {
            header("location:$redirecionar");
        }
    }

}